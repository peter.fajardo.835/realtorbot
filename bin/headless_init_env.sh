#!/bin/bash
set -e
IFS='|'

AWSCLOUDFORMATIONCONFIG="{\
\"configLevel\":\"project\",\
\"useProfile\":true,\
\"profileName\":\"default\"\
}"


SOCIALAUTH="{\"googleAppIdUserPool\":\"$GOOGLE_API_ID\",\
\"googleAppSecretUserPool\":\"$GOOGLE_APP_SECRET\",\
\"facebookAppIdUserPool\":\"$FACEBOOK_ID\",\
\"facebookAppSecretUserPool\":\"$FACEBOOK_APP_SECRET\"\
}\
}"

ENV_NAME="prod"
AMPLIFY="{\
\"envName\":\"$ENV_NAME\"\
}"

PROVIDERS="{\
\"awscloudformation\":$AWSCLOUDFORMATIONCONFIG\
}"

CATEGORIES="{\
\"auth\":$SOCIALAUTH}"

amplify init \
--amplify $AMPLIFY \
--providers $PROVIDERS \
--categories $CATEGORIES \
--yes
