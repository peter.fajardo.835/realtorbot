#!/usr/bin/env bash
set -e
IFS='|'

help_output () {
    echo "usage: amplify-pull <--environment|-e <name>>"
    echo "  --environment  The name of the Amplify environment to use"
    exit 1
}

ENV=""
IS_SIMPLE=false
POSITIONAL=()
while [[ $# -gt 0 ]]
    do
    key="$1"
    case ${key} in
        -e|--environment)
        ENV=$2
        shift
        ;;
        *)
        POSITIONAL+=("$1")
        shift
        ;;
    esac
done
set -- "${POSITIONAL[@]}"

# if no provided environment name, use default env variable, then user override
if [[ ${ENV} = "" ]];
then
    ENV=${AWS_BRANCH}
fi

if [[ ${USER_BRANCH} != "" ]];
then
    ENV=${USER_BRANCH}
fi

# strip slashes, limit to 10 chars
ENV=$(echo ${ENV} | sed 's;\\;;g' | sed 's;\/;;g' | cut -c -10)

# Check valid environment name
if [[ -z ${ENV} || "${ENV}" =~ [^a-zA-Z0-9\-]+ ]] ; then help_output ; fi

AMPLIFY="{\
\"appId\":\"${AWS_APP_ID}\",\
\"envName\":\"${ENV}\"\
}"
AWSCONFIG="{\
\"configLevel\":\"project\",\
\"useProfile\":true,\
\"profileName\":\"default\",\
\"AmplifyAppId\":\"${AWS_APP_ID}\"\
}"
PROVIDERS="{\
\"awscloudformation\":${AWSCONFIG}\
}"
# STACKINFO="$(amplify env get --json --name ${ENV})"
amplify pull --amplify ${AMPLIFY} --providers ${PROVIDERS} --yes
