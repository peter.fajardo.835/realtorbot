/* tslint:disable */
/* eslint-disable */
//  This file was automatically generated and should not be edited.
import { Injectable } from "@angular/core";
import API, { graphqlOperation } from "@aws-amplify/api";
import { GraphQLResult } from "@aws-amplify/api/lib/types";
import { Observable } from "zen-observable-ts";

export type CreateTeamInput = {
  name: string;
  members?: Array<string | null> | null;
};

export type ModelTeamConditionInput = {
  members?: ModelStringInput | null;
  and?: Array<ModelTeamConditionInput | null> | null;
  or?: Array<ModelTeamConditionInput | null> | null;
  not?: ModelTeamConditionInput | null;
};

export type ModelStringInput = {
  ne?: string | null;
  eq?: string | null;
  le?: string | null;
  lt?: string | null;
  ge?: string | null;
  gt?: string | null;
  contains?: string | null;
  notContains?: string | null;
  between?: Array<string | null> | null;
  beginsWith?: string | null;
  attributeExists?: boolean | null;
  attributeType?: ModelAttributeTypes | null;
  size?: ModelSizeInput | null;
};

export enum ModelAttributeTypes {
  binary = "binary",
  binarySet = "binarySet",
  bool = "bool",
  list = "list",
  map = "map",
  number = "number",
  numberSet = "numberSet",
  string = "string",
  stringSet = "stringSet",
  _null = "_null"
}

export type ModelSizeInput = {
  ne?: number | null;
  eq?: number | null;
  le?: number | null;
  lt?: number | null;
  ge?: number | null;
  gt?: number | null;
  between?: Array<number | null> | null;
};

export type UpdateTeamInput = {
  name: string;
  members?: Array<string | null> | null;
};

export type DeleteTeamInput = {
  name: string;
};

export type CreateListingInput = {
  id?: string | null;
  hash: string;
  address: string;
  zipcode: string;
  bedrooms?: number | null;
  full_bathroom?: number | null;
  half_bathroom?: number | null;
  owner: string;
};

export type ModelListingConditionInput = {
  hash?: ModelStringInput | null;
  address?: ModelStringInput | null;
  zipcode?: ModelStringInput | null;
  bedrooms?: ModelIntInput | null;
  full_bathroom?: ModelIntInput | null;
  half_bathroom?: ModelIntInput | null;
  and?: Array<ModelListingConditionInput | null> | null;
  or?: Array<ModelListingConditionInput | null> | null;
  not?: ModelListingConditionInput | null;
};

export type ModelIntInput = {
  ne?: number | null;
  eq?: number | null;
  le?: number | null;
  lt?: number | null;
  ge?: number | null;
  gt?: number | null;
  between?: Array<number | null> | null;
  attributeExists?: boolean | null;
  attributeType?: ModelAttributeTypes | null;
};

export type UpdateListingInput = {
  id: string;
  hash?: string | null;
  address?: string | null;
  zipcode?: string | null;
  bedrooms?: number | null;
  full_bathroom?: number | null;
  half_bathroom?: number | null;
  owner?: string | null;
};

export type DeleteListingInput = {
  id?: string | null;
};

export type UpdateCheckInsInput = {
  id: string;
  name?: string | null;
  email?: string | null;
  phone_number?: string | null;
  listingID?: string | null;
  owner?: string | null;
};

export type ModelCheckInsConditionInput = {
  name?: ModelStringInput | null;
  email?: ModelStringInput | null;
  phone_number?: ModelStringInput | null;
  listingID?: ModelIDInput | null;
  and?: Array<ModelCheckInsConditionInput | null> | null;
  or?: Array<ModelCheckInsConditionInput | null> | null;
  not?: ModelCheckInsConditionInput | null;
};

export type ModelIDInput = {
  ne?: string | null;
  eq?: string | null;
  le?: string | null;
  lt?: string | null;
  ge?: string | null;
  gt?: string | null;
  contains?: string | null;
  notContains?: string | null;
  between?: Array<string | null> | null;
  beginsWith?: string | null;
  attributeExists?: boolean | null;
  attributeType?: ModelAttributeTypes | null;
  size?: ModelSizeInput | null;
};

export type DeleteCheckInsInput = {
  id?: string | null;
};

export type UpdateCommentInput = {
  id: string;
  listingID?: string | null;
  email?: string | null;
  content?: string | null;
  owner?: string | null;
};

export type ModelCommentConditionInput = {
  listingID?: ModelIDInput | null;
  email?: ModelStringInput | null;
  content?: ModelStringInput | null;
  and?: Array<ModelCommentConditionInput | null> | null;
  or?: Array<ModelCommentConditionInput | null> | null;
  not?: ModelCommentConditionInput | null;
};

export type DeleteCommentInput = {
  id?: string | null;
};

export type CreateCheckInsInput = {
  id?: string | null;
  name: string;
  email: string;
  phone_number?: string | null;
  listingID: string;
  owner: string;
};

export type CreateCommentInput = {
  id?: string | null;
  listingID: string;
  email: string;
  content: string;
  owner: string;
};

export type ModelTeamFilterInput = {
  name?: ModelStringInput | null;
  members?: ModelStringInput | null;
  and?: Array<ModelTeamFilterInput | null> | null;
  or?: Array<ModelTeamFilterInput | null> | null;
  not?: ModelTeamFilterInput | null;
};

export enum ModelSortDirection {
  ASC = "ASC",
  DESC = "DESC"
}

export type ModelCheckInsFilterInput = {
  id?: ModelIDInput | null;
  name?: ModelStringInput | null;
  email?: ModelStringInput | null;
  phone_number?: ModelStringInput | null;
  listingID?: ModelIDInput | null;
  owner?: ModelStringInput | null;
  and?: Array<ModelCheckInsFilterInput | null> | null;
  or?: Array<ModelCheckInsFilterInput | null> | null;
  not?: ModelCheckInsFilterInput | null;
};

export type ModelCommentFilterInput = {
  id?: ModelIDInput | null;
  listingID?: ModelIDInput | null;
  email?: ModelStringInput | null;
  content?: ModelStringInput | null;
  owner?: ModelStringInput | null;
  and?: Array<ModelCommentFilterInput | null> | null;
  or?: Array<ModelCommentFilterInput | null> | null;
  not?: ModelCommentFilterInput | null;
};

export type ModelListingFilterInput = {
  id?: ModelIDInput | null;
  hash?: ModelStringInput | null;
  address?: ModelStringInput | null;
  zipcode?: ModelStringInput | null;
  bedrooms?: ModelIntInput | null;
  full_bathroom?: ModelIntInput | null;
  half_bathroom?: ModelIntInput | null;
  owner?: ModelStringInput | null;
  and?: Array<ModelListingFilterInput | null> | null;
  or?: Array<ModelListingFilterInput | null> | null;
  not?: ModelListingFilterInput | null;
};

export type CreateTeamMutation = {
  __typename: "Team";
  name: string;
  members: Array<string | null> | null;
  createdAt: string;
  updatedAt: string;
  owner: string | null;
};

export type UpdateTeamMutation = {
  __typename: "Team";
  name: string;
  members: Array<string | null> | null;
  createdAt: string;
  updatedAt: string;
  owner: string | null;
};

export type DeleteTeamMutation = {
  __typename: "Team";
  name: string;
  members: Array<string | null> | null;
  createdAt: string;
  updatedAt: string;
  owner: string | null;
};

export type CreateListingMutation = {
  __typename: "Listing";
  id: string;
  hash: string;
  address: string;
  zipcode: string;
  bedrooms: number | null;
  full_bathroom: number | null;
  half_bathroom: number | null;
  owner: string;
  checkin: {
    __typename: "ModelCheckInsConnection";
    items: Array<{
      __typename: "CheckIns";
      id: string;
      name: string;
      email: string;
      phone_number: string | null;
      listingID: string;
      owner: string;
      createdAt: string;
      updatedAt: string;
    } | null> | null;
    nextToken: string | null;
  } | null;
  createdAt: string;
  updatedAt: string;
};

export type UpdateListingMutation = {
  __typename: "Listing";
  id: string;
  hash: string;
  address: string;
  zipcode: string;
  bedrooms: number | null;
  full_bathroom: number | null;
  half_bathroom: number | null;
  owner: string;
  checkin: {
    __typename: "ModelCheckInsConnection";
    items: Array<{
      __typename: "CheckIns";
      id: string;
      name: string;
      email: string;
      phone_number: string | null;
      listingID: string;
      owner: string;
      createdAt: string;
      updatedAt: string;
    } | null> | null;
    nextToken: string | null;
  } | null;
  createdAt: string;
  updatedAt: string;
};

export type DeleteListingMutation = {
  __typename: "Listing";
  id: string;
  hash: string;
  address: string;
  zipcode: string;
  bedrooms: number | null;
  full_bathroom: number | null;
  half_bathroom: number | null;
  owner: string;
  checkin: {
    __typename: "ModelCheckInsConnection";
    items: Array<{
      __typename: "CheckIns";
      id: string;
      name: string;
      email: string;
      phone_number: string | null;
      listingID: string;
      owner: string;
      createdAt: string;
      updatedAt: string;
    } | null> | null;
    nextToken: string | null;
  } | null;
  createdAt: string;
  updatedAt: string;
};

export type UpdateCheckInsMutation = {
  __typename: "CheckIns";
  id: string;
  name: string;
  email: string;
  phone_number: string | null;
  listingID: string;
  owner: string;
  createdAt: string;
  updatedAt: string;
};

export type DeleteCheckInsMutation = {
  __typename: "CheckIns";
  id: string;
  name: string;
  email: string;
  phone_number: string | null;
  listingID: string;
  owner: string;
  createdAt: string;
  updatedAt: string;
};

export type UpdateCommentMutation = {
  __typename: "Comment";
  id: string;
  listingID: string;
  email: string;
  content: string;
  owner: string;
  createdAt: string;
  updatedAt: string;
};

export type DeleteCommentMutation = {
  __typename: "Comment";
  id: string;
  listingID: string;
  email: string;
  content: string;
  owner: string;
  createdAt: string;
  updatedAt: string;
};

export type CreateCheckInsMutation = {
  __typename: "CheckIns";
  id: string;
  name: string;
  email: string;
  phone_number: string | null;
  listingID: string;
  owner: string;
  createdAt: string;
  updatedAt: string;
};

export type CreateCommentMutation = {
  __typename: "Comment";
  id: string;
  listingID: string;
  email: string;
  content: string;
  owner: string;
  createdAt: string;
  updatedAt: string;
};

export type GetTeamQuery = {
  __typename: "Team";
  name: string;
  members: Array<string | null> | null;
  createdAt: string;
  updatedAt: string;
  owner: string | null;
};

export type ListTeamsQuery = {
  __typename: "ModelTeamConnection";
  items: Array<{
    __typename: "Team";
    name: string;
    members: Array<string | null> | null;
    createdAt: string;
    updatedAt: string;
    owner: string | null;
  } | null> | null;
  nextToken: string | null;
};

export type GetCheckInsQuery = {
  __typename: "CheckIns";
  id: string;
  name: string;
  email: string;
  phone_number: string | null;
  listingID: string;
  owner: string;
  createdAt: string;
  updatedAt: string;
};

export type ListCheckInssQuery = {
  __typename: "ModelCheckInsConnection";
  items: Array<{
    __typename: "CheckIns";
    id: string;
    name: string;
    email: string;
    phone_number: string | null;
    listingID: string;
    owner: string;
    createdAt: string;
    updatedAt: string;
  } | null> | null;
  nextToken: string | null;
};

export type GetCommentQuery = {
  __typename: "Comment";
  id: string;
  listingID: string;
  email: string;
  content: string;
  owner: string;
  createdAt: string;
  updatedAt: string;
};

export type ListCommentsQuery = {
  __typename: "ModelCommentConnection";
  items: Array<{
    __typename: "Comment";
    id: string;
    listingID: string;
    email: string;
    content: string;
    owner: string;
    createdAt: string;
    updatedAt: string;
  } | null> | null;
  nextToken: string | null;
};

export type GetListingQuery = {
  __typename: "Listing";
  id: string;
  hash: string;
  address: string;
  zipcode: string;
  bedrooms: number | null;
  full_bathroom: number | null;
  half_bathroom: number | null;
  owner: string;
  checkin: {
    __typename: "ModelCheckInsConnection";
    items: Array<{
      __typename: "CheckIns";
      id: string;
      name: string;
      email: string;
      phone_number: string | null;
      listingID: string;
      owner: string;
      createdAt: string;
      updatedAt: string;
    } | null> | null;
    nextToken: string | null;
  } | null;
  createdAt: string;
  updatedAt: string;
};

export type ListListingsQuery = {
  __typename: "ModelListingConnection";
  items: Array<{
    __typename: "Listing";
    id: string;
    hash: string;
    address: string;
    zipcode: string;
    bedrooms: number | null;
    full_bathroom: number | null;
    half_bathroom: number | null;
    owner: string;
    checkin: {
      __typename: "ModelCheckInsConnection";
      nextToken: string | null;
    } | null;
    createdAt: string;
    updatedAt: string;
  } | null> | null;
  nextToken: string | null;
};

export type OnCreateTeamSubscription = {
  __typename: "Team";
  name: string;
  members: Array<string | null> | null;
  createdAt: string;
  updatedAt: string;
  owner: string | null;
};

export type OnUpdateTeamSubscription = {
  __typename: "Team";
  name: string;
  members: Array<string | null> | null;
  createdAt: string;
  updatedAt: string;
  owner: string | null;
};

export type OnDeleteTeamSubscription = {
  __typename: "Team";
  name: string;
  members: Array<string | null> | null;
  createdAt: string;
  updatedAt: string;
  owner: string | null;
};

export type OnCreateCheckInsSubscription = {
  __typename: "CheckIns";
  id: string;
  name: string;
  email: string;
  phone_number: string | null;
  listingID: string;
  owner: string;
  createdAt: string;
  updatedAt: string;
};

export type OnUpdateCheckInsSubscription = {
  __typename: "CheckIns";
  id: string;
  name: string;
  email: string;
  phone_number: string | null;
  listingID: string;
  owner: string;
  createdAt: string;
  updatedAt: string;
};

export type OnDeleteCheckInsSubscription = {
  __typename: "CheckIns";
  id: string;
  name: string;
  email: string;
  phone_number: string | null;
  listingID: string;
  owner: string;
  createdAt: string;
  updatedAt: string;
};

export type OnCreateCommentSubscription = {
  __typename: "Comment";
  id: string;
  listingID: string;
  email: string;
  content: string;
  owner: string;
  createdAt: string;
  updatedAt: string;
};

export type OnUpdateCommentSubscription = {
  __typename: "Comment";
  id: string;
  listingID: string;
  email: string;
  content: string;
  owner: string;
  createdAt: string;
  updatedAt: string;
};

export type OnDeleteCommentSubscription = {
  __typename: "Comment";
  id: string;
  listingID: string;
  email: string;
  content: string;
  owner: string;
  createdAt: string;
  updatedAt: string;
};

export type OnCreateListingSubscription = {
  __typename: "Listing";
  id: string;
  hash: string;
  address: string;
  zipcode: string;
  bedrooms: number | null;
  full_bathroom: number | null;
  half_bathroom: number | null;
  owner: string;
  checkin: {
    __typename: "ModelCheckInsConnection";
    items: Array<{
      __typename: "CheckIns";
      id: string;
      name: string;
      email: string;
      phone_number: string | null;
      listingID: string;
      owner: string;
      createdAt: string;
      updatedAt: string;
    } | null> | null;
    nextToken: string | null;
  } | null;
  createdAt: string;
  updatedAt: string;
};

export type OnUpdateListingSubscription = {
  __typename: "Listing";
  id: string;
  hash: string;
  address: string;
  zipcode: string;
  bedrooms: number | null;
  full_bathroom: number | null;
  half_bathroom: number | null;
  owner: string;
  checkin: {
    __typename: "ModelCheckInsConnection";
    items: Array<{
      __typename: "CheckIns";
      id: string;
      name: string;
      email: string;
      phone_number: string | null;
      listingID: string;
      owner: string;
      createdAt: string;
      updatedAt: string;
    } | null> | null;
    nextToken: string | null;
  } | null;
  createdAt: string;
  updatedAt: string;
};

export type OnDeleteListingSubscription = {
  __typename: "Listing";
  id: string;
  hash: string;
  address: string;
  zipcode: string;
  bedrooms: number | null;
  full_bathroom: number | null;
  half_bathroom: number | null;
  owner: string;
  checkin: {
    __typename: "ModelCheckInsConnection";
    items: Array<{
      __typename: "CheckIns";
      id: string;
      name: string;
      email: string;
      phone_number: string | null;
      listingID: string;
      owner: string;
      createdAt: string;
      updatedAt: string;
    } | null> | null;
    nextToken: string | null;
  } | null;
  createdAt: string;
  updatedAt: string;
};

@Injectable({
  providedIn: "root"
})
export class APIService {
  async CreateTeam(
    input: CreateTeamInput,
    condition?: ModelTeamConditionInput
  ): Promise<CreateTeamMutation> {
    const statement = `mutation CreateTeam($input: CreateTeamInput!, $condition: ModelTeamConditionInput) {
        createTeam(input: $input, condition: $condition) {
          __typename
          name
          members
          createdAt
          updatedAt
          owner
        }
      }`;
    const gqlAPIServiceArguments: any = {
      input
    };
    if (condition) {
      gqlAPIServiceArguments.condition = condition;
    }
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <CreateTeamMutation>response.data.createTeam;
  }
  async UpdateTeam(
    input: UpdateTeamInput,
    condition?: ModelTeamConditionInput
  ): Promise<UpdateTeamMutation> {
    const statement = `mutation UpdateTeam($input: UpdateTeamInput!, $condition: ModelTeamConditionInput) {
        updateTeam(input: $input, condition: $condition) {
          __typename
          name
          members
          createdAt
          updatedAt
          owner
        }
      }`;
    const gqlAPIServiceArguments: any = {
      input
    };
    if (condition) {
      gqlAPIServiceArguments.condition = condition;
    }
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <UpdateTeamMutation>response.data.updateTeam;
  }
  async DeleteTeam(
    input: DeleteTeamInput,
    condition?: ModelTeamConditionInput
  ): Promise<DeleteTeamMutation> {
    const statement = `mutation DeleteTeam($input: DeleteTeamInput!, $condition: ModelTeamConditionInput) {
        deleteTeam(input: $input, condition: $condition) {
          __typename
          name
          members
          createdAt
          updatedAt
          owner
        }
      }`;
    const gqlAPIServiceArguments: any = {
      input
    };
    if (condition) {
      gqlAPIServiceArguments.condition = condition;
    }
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <DeleteTeamMutation>response.data.deleteTeam;
  }
  async CreateListing(
    input: CreateListingInput,
    condition?: ModelListingConditionInput
  ): Promise<CreateListingMutation> {
    const statement = `mutation CreateListing($input: CreateListingInput!, $condition: ModelListingConditionInput) {
        createListing(input: $input, condition: $condition) {
          __typename
          id
          hash
          address
          zipcode
          bedrooms
          full_bathroom
          half_bathroom
          owner
          checkin {
            __typename
            items {
              __typename
              id
              name
              email
              phone_number
              listingID
              owner
              createdAt
              updatedAt
            }
            nextToken
          }
          createdAt
          updatedAt
        }
      }`;
    const gqlAPIServiceArguments: any = {
      input
    };
    if (condition) {
      gqlAPIServiceArguments.condition = condition;
    }
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <CreateListingMutation>response.data.createListing;
  }
  async UpdateListing(
    input: UpdateListingInput,
    condition?: ModelListingConditionInput
  ): Promise<UpdateListingMutation> {
    const statement = `mutation UpdateListing($input: UpdateListingInput!, $condition: ModelListingConditionInput) {
        updateListing(input: $input, condition: $condition) {
          __typename
          id
          hash
          address
          zipcode
          bedrooms
          full_bathroom
          half_bathroom
          owner
          checkin {
            __typename
            items {
              __typename
              id
              name
              email
              phone_number
              listingID
              owner
              createdAt
              updatedAt
            }
            nextToken
          }
          createdAt
          updatedAt
        }
      }`;
    const gqlAPIServiceArguments: any = {
      input
    };
    if (condition) {
      gqlAPIServiceArguments.condition = condition;
    }
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <UpdateListingMutation>response.data.updateListing;
  }
  async DeleteListing(
    input: DeleteListingInput,
    condition?: ModelListingConditionInput
  ): Promise<DeleteListingMutation> {
    const statement = `mutation DeleteListing($input: DeleteListingInput!, $condition: ModelListingConditionInput) {
        deleteListing(input: $input, condition: $condition) {
          __typename
          id
          hash
          address
          zipcode
          bedrooms
          full_bathroom
          half_bathroom
          owner
          checkin {
            __typename
            items {
              __typename
              id
              name
              email
              phone_number
              listingID
              owner
              createdAt
              updatedAt
            }
            nextToken
          }
          createdAt
          updatedAt
        }
      }`;
    const gqlAPIServiceArguments: any = {
      input
    };
    if (condition) {
      gqlAPIServiceArguments.condition = condition;
    }
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <DeleteListingMutation>response.data.deleteListing;
  }
  async UpdateCheckIns(
    input: UpdateCheckInsInput,
    condition?: ModelCheckInsConditionInput
  ): Promise<UpdateCheckInsMutation> {
    const statement = `mutation UpdateCheckIns($input: UpdateCheckInsInput!, $condition: ModelCheckInsConditionInput) {
        updateCheckIns(input: $input, condition: $condition) {
          __typename
          id
          name
          email
          phone_number
          listingID
          owner
          createdAt
          updatedAt
        }
      }`;
    const gqlAPIServiceArguments: any = {
      input
    };
    if (condition) {
      gqlAPIServiceArguments.condition = condition;
    }
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <UpdateCheckInsMutation>response.data.updateCheckIns;
  }
  async DeleteCheckIns(
    input: DeleteCheckInsInput,
    condition?: ModelCheckInsConditionInput
  ): Promise<DeleteCheckInsMutation> {
    const statement = `mutation DeleteCheckIns($input: DeleteCheckInsInput!, $condition: ModelCheckInsConditionInput) {
        deleteCheckIns(input: $input, condition: $condition) {
          __typename
          id
          name
          email
          phone_number
          listingID
          owner
          createdAt
          updatedAt
        }
      }`;
    const gqlAPIServiceArguments: any = {
      input
    };
    if (condition) {
      gqlAPIServiceArguments.condition = condition;
    }
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <DeleteCheckInsMutation>response.data.deleteCheckIns;
  }
  async UpdateComment(
    input: UpdateCommentInput,
    condition?: ModelCommentConditionInput
  ): Promise<UpdateCommentMutation> {
    const statement = `mutation UpdateComment($input: UpdateCommentInput!, $condition: ModelCommentConditionInput) {
        updateComment(input: $input, condition: $condition) {
          __typename
          id
          listingID
          email
          content
          owner
          createdAt
          updatedAt
        }
      }`;
    const gqlAPIServiceArguments: any = {
      input
    };
    if (condition) {
      gqlAPIServiceArguments.condition = condition;
    }
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <UpdateCommentMutation>response.data.updateComment;
  }
  async DeleteComment(
    input: DeleteCommentInput,
    condition?: ModelCommentConditionInput
  ): Promise<DeleteCommentMutation> {
    const statement = `mutation DeleteComment($input: DeleteCommentInput!, $condition: ModelCommentConditionInput) {
        deleteComment(input: $input, condition: $condition) {
          __typename
          id
          listingID
          email
          content
          owner
          createdAt
          updatedAt
        }
      }`;
    const gqlAPIServiceArguments: any = {
      input
    };
    if (condition) {
      gqlAPIServiceArguments.condition = condition;
    }
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <DeleteCommentMutation>response.data.deleteComment;
  }
  async CreateCheckIns(
    input: CreateCheckInsInput,
    condition?: ModelCheckInsConditionInput
  ): Promise<CreateCheckInsMutation> {
    const statement = `mutation CreateCheckIns($input: CreateCheckInsInput!, $condition: ModelCheckInsConditionInput) {
        createCheckIns(input: $input, condition: $condition) {
          __typename
          id
          name
          email
          phone_number
          listingID
          owner
          createdAt
          updatedAt
        }
      }`;
    const gqlAPIServiceArguments: any = {
      input
    };
    if (condition) {
      gqlAPIServiceArguments.condition = condition;
    }
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <CreateCheckInsMutation>response.data.createCheckIns;
  }
  async CreateComment(
    input: CreateCommentInput,
    condition?: ModelCommentConditionInput
  ): Promise<CreateCommentMutation> {
    const statement = `mutation CreateComment($input: CreateCommentInput!, $condition: ModelCommentConditionInput) {
        createComment(input: $input, condition: $condition) {
          __typename
          id
          listingID
          email
          content
          owner
          createdAt
          updatedAt
        }
      }`;
    const gqlAPIServiceArguments: any = {
      input
    };
    if (condition) {
      gqlAPIServiceArguments.condition = condition;
    }
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <CreateCommentMutation>response.data.createComment;
  }
  async GetTeam(name: string): Promise<GetTeamQuery> {
    const statement = `query GetTeam($name: String!) {
        getTeam(name: $name) {
          __typename
          name
          members
          createdAt
          updatedAt
          owner
        }
      }`;
    const gqlAPIServiceArguments: any = {
      name
    };
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <GetTeamQuery>response.data.getTeam;
  }
  async ListTeams(
    name?: string,
    filter?: ModelTeamFilterInput,
    limit?: number,
    nextToken?: string,
    sortDirection?: ModelSortDirection
  ): Promise<ListTeamsQuery> {
    const statement = `query ListTeams($name: String, $filter: ModelTeamFilterInput, $limit: Int, $nextToken: String, $sortDirection: ModelSortDirection) {
        listTeams(name: $name, filter: $filter, limit: $limit, nextToken: $nextToken, sortDirection: $sortDirection) {
          __typename
          items {
            __typename
            name
            members
            createdAt
            updatedAt
            owner
          }
          nextToken
        }
      }`;
    const gqlAPIServiceArguments: any = {};
    if (name) {
      gqlAPIServiceArguments.name = name;
    }
    if (filter) {
      gqlAPIServiceArguments.filter = filter;
    }
    if (limit) {
      gqlAPIServiceArguments.limit = limit;
    }
    if (nextToken) {
      gqlAPIServiceArguments.nextToken = nextToken;
    }
    if (sortDirection) {
      gqlAPIServiceArguments.sortDirection = sortDirection;
    }
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <ListTeamsQuery>response.data.listTeams;
  }
  async GetCheckIns(id: string): Promise<GetCheckInsQuery> {
    const statement = `query GetCheckIns($id: ID!) {
        getCheckIns(id: $id) {
          __typename
          id
          name
          email
          phone_number
          listingID
          owner
          createdAt
          updatedAt
        }
      }`;
    const gqlAPIServiceArguments: any = {
      id
    };
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <GetCheckInsQuery>response.data.getCheckIns;
  }
  async ListCheckInss(
    filter?: ModelCheckInsFilterInput,
    limit?: number,
    nextToken?: string
  ): Promise<ListCheckInssQuery> {
    const statement = `query ListCheckInss($filter: ModelCheckInsFilterInput, $limit: Int, $nextToken: String) {
        listCheckInss(filter: $filter, limit: $limit, nextToken: $nextToken) {
          __typename
          items {
            __typename
            id
            name
            email
            phone_number
            listingID
            owner
            createdAt
            updatedAt
          }
          nextToken
        }
      }`;
    const gqlAPIServiceArguments: any = {};
    if (filter) {
      gqlAPIServiceArguments.filter = filter;
    }
    if (limit) {
      gqlAPIServiceArguments.limit = limit;
    }
    if (nextToken) {
      gqlAPIServiceArguments.nextToken = nextToken;
    }
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <ListCheckInssQuery>response.data.listCheckInss;
  }
  async GetComment(id: string): Promise<GetCommentQuery> {
    const statement = `query GetComment($id: ID!) {
        getComment(id: $id) {
          __typename
          id
          listingID
          email
          content
          owner
          createdAt
          updatedAt
        }
      }`;
    const gqlAPIServiceArguments: any = {
      id
    };
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <GetCommentQuery>response.data.getComment;
  }
  async ListComments(
    filter?: ModelCommentFilterInput,
    limit?: number,
    nextToken?: string
  ): Promise<ListCommentsQuery> {
    const statement = `query ListComments($filter: ModelCommentFilterInput, $limit: Int, $nextToken: String) {
        listComments(filter: $filter, limit: $limit, nextToken: $nextToken) {
          __typename
          items {
            __typename
            id
            listingID
            email
            content
            owner
            createdAt
            updatedAt
          }
          nextToken
        }
      }`;
    const gqlAPIServiceArguments: any = {};
    if (filter) {
      gqlAPIServiceArguments.filter = filter;
    }
    if (limit) {
      gqlAPIServiceArguments.limit = limit;
    }
    if (nextToken) {
      gqlAPIServiceArguments.nextToken = nextToken;
    }
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <ListCommentsQuery>response.data.listComments;
  }
  async GetListing(id: string): Promise<GetListingQuery> {
    const statement = `query GetListing($id: ID!) {
        getListing(id: $id) {
          __typename
          id
          hash
          address
          zipcode
          bedrooms
          full_bathroom
          half_bathroom
          owner
          checkin {
            __typename
            items {
              __typename
              id
              name
              email
              phone_number
              listingID
              owner
              createdAt
              updatedAt
            }
            nextToken
          }
          createdAt
          updatedAt
        }
      }`;
    const gqlAPIServiceArguments: any = {
      id
    };
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <GetListingQuery>response.data.getListing;
  }
  async ListListings(
    filter?: ModelListingFilterInput,
    limit?: number,
    nextToken?: string
  ): Promise<ListListingsQuery> {
    const statement = `query ListListings($filter: ModelListingFilterInput, $limit: Int, $nextToken: String) {
        listListings(filter: $filter, limit: $limit, nextToken: $nextToken) {
          __typename
          items {
            __typename
            id
            hash
            address
            zipcode
            bedrooms
            full_bathroom
            half_bathroom
            owner
            checkin {
              __typename
              nextToken
            }
            createdAt
            updatedAt
          }
          nextToken
        }
      }`;
    const gqlAPIServiceArguments: any = {};
    if (filter) {
      gqlAPIServiceArguments.filter = filter;
    }
    if (limit) {
      gqlAPIServiceArguments.limit = limit;
    }
    if (nextToken) {
      gqlAPIServiceArguments.nextToken = nextToken;
    }
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <ListListingsQuery>response.data.listListings;
  }
  OnCreateTeamListener: Observable<OnCreateTeamSubscription> = API.graphql(
    graphqlOperation(
      `subscription OnCreateTeam($owner: String!) {
        onCreateTeam(owner: $owner) {
          __typename
          name
          members
          createdAt
          updatedAt
          owner
        }
      }`
    )
  ) as unknown as Observable<OnCreateTeamSubscription>;

  OnUpdateTeamListener: Observable<OnUpdateTeamSubscription> = API.graphql(
    graphqlOperation(
      `subscription OnUpdateTeam($owner: String!) {
        onUpdateTeam(owner: $owner) {
          __typename
          name
          members
          createdAt
          updatedAt
          owner
        }
      }`
    )
  ) as unknown as Observable<OnUpdateTeamSubscription>;

  OnDeleteTeamListener: Observable<OnDeleteTeamSubscription> = API.graphql(
    graphqlOperation(
      `subscription OnDeleteTeam($owner: String!) {
        onDeleteTeam(owner: $owner) {
          __typename
          name
          members
          createdAt
          updatedAt
          owner
        }
      }`
    )
  ) as unknown as Observable<OnDeleteTeamSubscription>;

  OnCreateCheckInsListener: Observable<
    OnCreateCheckInsSubscription
  > = API.graphql(
      graphqlOperation(
        `subscription OnCreateCheckIns($owner: String!) {
        onCreateCheckIns(owner: $owner) {
          __typename
          id
          name
          email
          phone_number
          listingID
          owner
          createdAt
          updatedAt
        }
      }`
      )
    ) as unknown as Observable<OnCreateCheckInsSubscription>;

  OnUpdateCheckInsListener: Observable<
    OnUpdateCheckInsSubscription
  > = API.graphql(
      graphqlOperation(
        `subscription OnUpdateCheckIns($owner: String!) {
        onUpdateCheckIns(owner: $owner) {
          __typename
          id
          name
          email
          phone_number
          listingID
          owner
          createdAt
          updatedAt
        }
      }`
      )
    ) as unknown as Observable<OnUpdateCheckInsSubscription>;

  OnDeleteCheckInsListener: Observable<
    OnDeleteCheckInsSubscription
  > = API.graphql(
      graphqlOperation(
        `subscription OnDeleteCheckIns($owner: String!) {
        onDeleteCheckIns(owner: $owner) {
          __typename
          id
          name
          email
          phone_number
          listingID
          owner
          createdAt
          updatedAt
        }
      }`
      )
    ) as unknown as Observable<OnDeleteCheckInsSubscription>;

  OnCreateCommentListener: Observable<
    OnCreateCommentSubscription
  > = API.graphql(
      graphqlOperation(
        `subscription OnCreateComment($owner: String!) {
        onCreateComment(owner: $owner) {
          __typename
          id
          listingID
          email
          content
          owner
          createdAt
          updatedAt
        }
      }`
      )
    ) as unknown as Observable<OnCreateCommentSubscription>;

  OnUpdateCommentListener: Observable<
    OnUpdateCommentSubscription
  > = API.graphql(
      graphqlOperation(
        `subscription OnUpdateComment($owner: String!) {
        onUpdateComment(owner: $owner) {
          __typename
          id
          listingID
          email
          content
          owner
          createdAt
          updatedAt
        }
      }`
      )
    ) as unknown as Observable<OnUpdateCommentSubscription>;

  OnDeleteCommentListener: Observable<
    OnDeleteCommentSubscription
  > = API.graphql(
      graphqlOperation(
        `subscription OnDeleteComment($owner: String!) {
        onDeleteComment(owner: $owner) {
          __typename
          id
          listingID
          email
          content
          owner
          createdAt
          updatedAt
        }
      }`
      )
    ) as unknown as Observable<OnDeleteCommentSubscription>;

  OnCreateListingListener: Observable<
    OnCreateListingSubscription
  > = API.graphql(
      graphqlOperation(
        `subscription OnCreateListing($owner: String!) {
        onCreateListing(owner: $owner) {
          __typename
          id
          hash
          address
          zipcode
          bedrooms
          full_bathroom
          half_bathroom
          owner
          checkin {
            __typename
            items {
              __typename
              id
              name
              email
              phone_number
              listingID
              owner
              createdAt
              updatedAt
            }
            nextToken
          }
          createdAt
          updatedAt
        }
      }`
      )
    ) as unknown as Observable<OnCreateListingSubscription>;

  OnUpdateListingListener: Observable<
    OnUpdateListingSubscription
  > = API.graphql(
      graphqlOperation(
        `subscription OnUpdateListing($owner: String!) {
        onUpdateListing(owner: $owner) {
          __typename
          id
          hash
          address
          zipcode
          bedrooms
          full_bathroom
          half_bathroom
          owner
          checkin {
            __typename
            items {
              __typename
              id
              name
              email
              phone_number
              listingID
              owner
              createdAt
              updatedAt
            }
            nextToken
          }
          createdAt
          updatedAt
        }
      }`
      )
    ) as unknown as Observable<OnUpdateListingSubscription>;

  OnDeleteListingListener: Observable<
    OnDeleteListingSubscription
  > = API.graphql(
      graphqlOperation(
        `subscription OnDeleteListing($owner: String!) {
        onDeleteListing(owner: $owner) {
          __typename
          id
          hash
          address
          zipcode
          bedrooms
          full_bathroom
          half_bathroom
          owner
          checkin {
            __typename
            items {
              __typename
              id
              name
              email
              phone_number
              listingID
              owner
              createdAt
              updatedAt
            }
            nextToken
          }
          createdAt
          updatedAt
        }
      }`
      )
    ) as unknown as Observable<OnDeleteListingSubscription>;
}
