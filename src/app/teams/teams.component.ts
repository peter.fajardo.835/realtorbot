import { Component, OnInit } from '@angular/core';
import { Logger } from 'aws-amplify';
import { NgForm } from '@angular/forms';
import { Router } from "@angular/router";
import { APIService } from '../API.service';
import Auth, { CognitoUser } from "@aws-amplify/auth";
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition, } from '@angular/material/snack-bar';

@Component({
  selector: 'app-teams',
  templateUrl: './teams.component.html'
})
export class TeamsComponent implements OnInit {
  profile: any = {};
  user: CognitoUser = null;
  username: string = null;
  teams: any;
  teamName: string = null;
  newTeam: any = {};
  testItems: any;

  constructor(
    private api: APIService,
    private _router: Router,
    private _snackBar: MatSnackBar){}

  async ngOnInit() {
    this.getUserInfo().then(() => {
      if (this.profile != null) {
        this.username = this.profile.attributes.email;
      } else {
        this._router.navigate(["/login"]);
      }
    });

    const gettingTeams = this.api.ListTeams();
    gettingTeams.then((evt) => {
      this.teams = evt.items;
    })
    .catch(error => {
      console.log(error)
      this.openSnackBar('Error getting teams.', 'error');
    });
    console.log('postTeam');

  }

  async getUserInfo() {
    this.profile = await Auth.currentUserInfo();
  }

  async createTeam( teamName: string ) {
    return this.api.CreateTeam({
      name: teamName,
      members: []
    });
  }

  onSubmit(form: NgForm) {
    const logger = new Logger('teams.onSubmit');
    logger.debug(form);
    logger.debug(`Value: ${form.value.addTeamName}`);
    this.teamName = form.value.addTeamName;
    this.createTeam( form.value.addTeamName )
    .then((resp: any) => {
      logger.debug(`createTeam`);
      this.newTeam = resp;
      this.openSnackBar('Success creating team.', 'success');
    })
    .catch(error => {
      logger.error(error);
      this.openSnackBar('Error creating team.', 'error');
    });
    // this.newTeam = await API.graphql(graphqlOperation(mutations.createTeam, {input: teamName}));

    // this.api.CreateTeam(teamName).then(() => {
    //    this.getTeams();}
    // )
  } // onSubmit

  openSnackBar(msg: string, type: string = 'error') {
    let duration = 5000;
    let h_pos: MatSnackBarHorizontalPosition = 'center';
    let v_pos: MatSnackBarVerticalPosition = 'top';
    switch(type) {
      case 'error': {
        duration = 5000;
        h_pos = 'center';
        v_pos = 'top';
        break;
      }
      case 'success': {
        duration = 5000;
        h_pos = 'center';
        v_pos = 'bottom';
        break;
      }
      case 'warn': {
        duration = 5000;
        h_pos = 'center';
        v_pos = 'bottom';
        break;
      }
      default: {
        duration = 5000;
        h_pos = 'center';
        v_pos = 'top';
        break;
      }
    };
    this._snackBar.open(msg, 'dismiss', {
      duration: duration,
      horizontalPosition: h_pos,
      verticalPosition: v_pos,
    });
  } // openSnackBar
}
