import Auth from "@aws-amplify/auth";
import sha256 from 'crypto-js/sha256';
import { paths } from '../app-paths';
import { APIService } from '../API.service';
import { API, Logger } from 'aws-amplify';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition, } from '@angular/material/snack-bar';

export interface Listing {
  id: string;
  address: string;
  zipcode: string;
  hash?: string;
  comments?: number;
  owner?: string;
  bedrooms?: number;
  full_bathroom?: 0;
  half_bathroom?: 0;
  url?: string;
  images?: Array<string>;
}

@Component({
  selector: 'app-property',
  templateUrl: './property.component.html',
  styleUrls: ['./property.component.scss'],
})
export class PropertyComponent implements OnInit {
  editing: Boolean = false;
  loading: Boolean = false;
  images: Array<string> = [];
  listing: Listing = {
    id: null,
    address: null,
    owner: null,
    hash: null,
    zipcode: null,
    url: null,
    bedrooms: 0,
    full_bathroom: 0,
    half_bathroom: 0,
    // checkins: [''],
    images: [],
    comments: 0
  };
  listingDataGroup: FormGroup;
  systemDataGroup: FormGroup;
  addressControl: FormControl = new FormControl({value: this.listing.address, disabled: true});
  zipcodeControl: FormControl = new FormControl({value: this.listing.zipcode, disabled: true});
  fullBathControl: FormControl = new FormControl({value: this.listing.full_bathroom, disabled: true});
  halfBathControl: FormControl = new FormControl({value: this.listing.half_bathroom, disabled: true});
  bedroomControl: FormControl = new FormControl({value: this.listing.bedrooms, disabled: true});
  hashControl: FormControl = new FormControl({value: this.listing.hash, disabled: true});
  urlControl: FormControl = new FormControl({value: this.listing.url, disabled: true});
  ownerControl: FormControl = new FormControl({value: this.listing.owner, disabled: true});
  propertyId: string = '';

  constructor(
    private api: APIService,
    private route: ActivatedRoute,
    private _router: Router,
    private fb: FormBuilder,
    private _snackBar: MatSnackBar){}

  ngOnInit() {
    const logger = new Logger('property.Init');
    this.loading = true;
    this.listingDataGroup = this.fb.group({
        address: this.addressControl,
        zipcode: this.zipcodeControl,
        bedrooms: this.bedroomControl,
        full_bathroom: this.fullBathControl,
        half_bathroom: this.halfBathControl,
      });
    this.systemDataGroup = this.fb.group({
        hash: this.hashControl,
        url: this.urlControl,
        owner: this.ownerControl,
    });
    this.images = [
        'https://image.shutterstock.com/image-photo/beautiful-exterior-newly-built-luxury-600w-529108441.jpg',
        'https://image.shutterstock.com/image-photo/big-custom-made-luxury-house-600w-1677499762.jpg',
        'https://charlotteagenda-charlotteagenda.netdna-ssl.com/wp-content/uploads/2020/05/real-estate-listing-front-porches.jpg'
      ];
    // get listing record hash
    const hash: string = this.route.snapshot.paramMap.get('hash');
    logger.debug(`Param Hash: ${hash}`);
    // get listing
    const listingPromise = this.listingsAPI(hash);
    // process getListing
    // @ts-ignore
    listingPromise.then((evt) => {
      logger.debug('ListingPromise');
      logger.debug(evt);
    // if no results, then this is not a proper record
    // @ts-ignore
      if (evt.data.listListings.items.length != 1) {
        logger.error(`Wrong number of listings for this Hash: %{hash}`);
        this._router.navigate(["/404"]);
      };
    // @ts-ignore
      logger.debug(evt.data.listListings.items[0]);
    // @ts-ignore
      this.listing = evt.data.listListings.items[0];
      this.listing.url = `${document.baseURI}${paths.visit}/${this.listing.hash}`;
      this.refreshListing(this.listing);
      this.loading = false;
    })
    .catch(error => {
      logger.debug(error);
      if (error.data.listListings && error.data.listListings.items.length > 0) {
        this.listing = error.data.listListings.items[0];
        this.refreshListing(this.listing);
      } else {
        logger.error(error);
        this.openSnackBar("Get Listing Failure!", 'error');
        this._router.navigate(["/404"]);
      }
      this.loading = false;
    });
  }

  refreshListing(listing: Listing) {
      this.editing = false;
      this.addressControl.reset({value: listing.address, disabled: !this.editing});
      this.zipcodeControl.reset({value: listing.zipcode, disabled: !this.editing});
      this.bedroomControl.reset({value: listing.bedrooms, disabled: !this.editing});
      this.fullBathControl.reset({value: listing.full_bathroom, disabled: !this.editing});
      this.halfBathControl.reset({value: listing.half_bathroom, disabled: !this.editing});
      this.hashControl.reset({value: listing.hash, disabled: true});
      this.urlControl.reset({value: `${document.baseURI}${paths.visit}/${listing.hash}`, disabled: true});
      this.ownerControl.reset({value: listing.owner, disabled: true});
  }

  updateComments(listing: Listing) {
  }

  async listingsAPI(hashId: string){
    const results = await API.graphql({
      query: `query ListListings($filter: ModelListingFilterInput, $limit: Int, $nextToken: String) {
        listListings(filter: $filter, limit: $limit, nextToken: $nextToken) {
          __typename
          items {
            __typename
            id
            hash
            address
            zipcode
            bedrooms
            full_bathroom
            half_bathroom
            owner
            checkin {
              __typename
              nextToken
            }
            createdAt
            updatedAt
          }
          nextToken
        }
      }`,
      variables: {filter: {hash: {eq: hashId}}},
      // @ts-ignore
      authMode: Auth.currentAuthenticatedUser() ? 'AMAZON_COGNITO_USER_POOLS' : 'AWS_IAM'
    })
    return results;
  }

  toggleEdit(form){
    const logger = new Logger('property.toggleEdit');
    logger.debug(form);
    this.editing = !this.editing;
    if (!this.editing) {
      //When done editing
      this.editing = false;
      logger.debug(`editing: ${this.editing}`);
      if (this.listing.address != form.value.address ||
        this.listing.zipcode != form.value.zipcode ||
        this.listing.bedrooms != form.value.bedrooms ||
        this.listing.full_bathroom != form.value.full_bathroom ||
        this.listing.half_bathroom != form.value.half_bathroom
      ) {
        // Update Listing API
        const newListing = {
          id: this.listing.id,
          address: form.value.address,
          zipcode: form.value.zipcode,
          bedrooms: form.value.bedrooms,
          full_bathroom: form.value.full_bathroom,
          half_bathroom: form.value.half_bathroom
        };
        this.loading = true;
        const updateListing = this.updateListingAPI(newListing);
        updateListing.then((evt) => {
          logger.debug('updateListing');
          logger.debug(evt);
          this.openSnackBar('Listing Updated', 'success');
          // @ts-ignore
          this.refreshListing(evt);
          this.loading = false;
        })
        .catch(error => {
          logger.error(error);
          this.refreshListing(this.listing);
          this.openSnackBar('Listing Failed to Update', 'error');
          this.loading = false;
        });
      }
    } else {
      // when editing
      logger.debug(`editing: ${this.editing}`);
      this.addressControl.enable();
      this.zipcodeControl.enable();
      this.bedroomControl.enable();
      this.fullBathControl.enable();
      this.halfBathControl.enable();

    }
  }

  updateListingAPI(listing: Listing) {
      const logger = new Logger('property.updateListingAPI');
      logger.debug(listing);
      return this.api.UpdateListing({
        id: listing.id,
        address: listing.address,
        zipcode: listing.zipcode,
        bedrooms: listing.bedrooms,
        full_bathroom: listing.full_bathroom,
        half_bathroom: listing.half_bathroom,
      });
  }

  refreshQRCode() {
    const logger = new Logger('property.refreshQRCode');
    // TODO regenerate QR Code
    const hash = sha256(`${Date.now()}::${this.listing.address}`);
    const updateQRCodePromise = this.rebuildQRCodeAPI(this.listing);
    updateQRCodePromise.then((evt) => {
      logger.debug(evt);
      this.listing.hash = evt.hash;
      this.refreshListing(this.listing);
      this.editing = false;
      this.openSnackBar('Refresh QR Code', 'success')
      this._router.navigate([`/property/${this.listing.hash}`]);
    })
    .catch(error => {
      logger.error(error);
      this.openSnackBar('Error occurred during QR Code Refresh');
    })
  } //refreshQRCode

  async rebuildQRCodeAPI (listing: Listing){
      return this.api.UpdateListing({
        id: listing.id,
        hash: `${this.getHash(listing.address)}`
      })
    } //rebuildQRCodeAPI

  getHash ( msg: string ) {
    return sha256(`${Date.now()}::${msg}`);
  } //getHash

  openSnackBar(msg: string, type: string = 'error') {
    let duration = 5000;
    let h_pos: MatSnackBarHorizontalPosition = 'center';
    let v_pos: MatSnackBarVerticalPosition = 'top';
    switch(type) {
      case 'error': {
        duration = 5000;
        h_pos = 'center';
        v_pos = 'top';
        break;
      }
      case 'success': {
        duration = 5000;
        h_pos = 'center';
        v_pos = 'bottom';
        break;
      }
      case 'warn': {
        duration = 5000;
        h_pos = 'center';
        v_pos = 'bottom';
        break;
      }
      default: {
        duration = 5000;
        h_pos = 'center';
        v_pos = 'top';
        break;
      }
    };
    this._snackBar.open(msg, 'dismiss', {
      duration: duration,
      horizontalPosition: h_pos,
      verticalPosition: v_pos,
    });
  } // openSnackBar
} //end component
