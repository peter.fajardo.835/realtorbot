import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { MatIconRegistry } from '@angular/material/icon';
import { Router } from "@angular/router";
import { Hub, Logger } from 'aws-amplify';
import { Auth } from 'aws-amplify';


@Component({
  selector: 'app-top-bar',
  templateUrl: './top-bar.component.html',
  styleUrls: ['./top-bar.component.scss']
})
export class TopBarComponent implements OnInit {

  constructor(private router: Router) {
  }
  loggedin: boolean = false;
  profile:any = {};
  username: string = null;

  listener = (data) => {
    const logger = new Logger('TopBarListener');
    switch (data.payload.event) {
      case 'signIn':
        this.loggedin = true;
        logger.info('user signed in');
        break;
      case 'signOut':
        this.loggedin = false;
        logger.info('user signed out');
        break;
    }
  }


  ngOnInit() {
    const logger = new Logger('TopBarInit');
    this.getUserInfo().then(() => {
      if (this.profile != null) {
        this.username = this.profile.attributes.email;
        this.loggedin = true;
      }
    }).
    catch(err => {
      logger.warn(err);
    });
    Hub.listen('auth', this.listener);
  }

  onLogOut() {
    const logger = new Logger('TopBarLogOut');
    Auth.signOut()
      .then(() => {
        this.loggedin = false;
        this.router.navigate(["/login"]);
      })
      .catch(err => logger.error(err));
  }

  async getUserInfo() {
    this.profile = await Auth.currentUserInfo();
  }

}

// async function signUp() {
//   try {
//     const user = await Auth.signUp({
//       username,
//       password,
//       attributes: {
//         email,
//         phone_number
//       }
//     });
//     console.log({ user });
//   } catch (error) {
//     console.log('error signing up:', error);
//   }
// }

// async function confirmSignUp() {
//     try {
//       await Auth.confirmSignUp(username, code);
//     } catch (error) {
//         console.log('error confirming sign up', error);
//     }
// }
