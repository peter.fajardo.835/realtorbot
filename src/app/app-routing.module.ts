import { NgModule } from '@angular/core';
import { paths } from './app-paths';
import { Routes, RouterModule } from '@angular/router';
import { TeamsComponent } from './teams/teams.component';
import { VisitComponent } from './visit/visit.component';
import { ListingsComponent } from './listings/listings.component';
import { PropertyComponent } from './property/property.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { PageNotFoundComponent } from './pagenotfound/pagenotfound.component';
import { UserAuthenticationComponent } from './auth/user-authentication.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: paths.login
  },
  {
    path: paths.dashboard,
    component: DashboardComponent
  },
  {
    path: paths.login,
    component: UserAuthenticationComponent
  },
  {
    path: paths.listings,
    component: ListingsComponent
  },
  {
    path: paths.teams,
    component: TeamsComponent
  },
  {
    path: `${paths.property}/:hash`,
    component: PropertyComponent
  },
  {
    path: `${paths.visit}/:hash`,
    component: VisitComponent
  },
  // {
  //   path: 'profile',
  //   component: ProfileComponent
  // },
  // {
  //   path: 'about',
  //   component: AboutComponent
  // },
  // {
  //   path: 'support',
  //   component: SupportComponent
  // },
  {
    path: '**',
    component: PageNotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
