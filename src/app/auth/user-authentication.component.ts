import { Component } from '@angular/core';
import { Auth } from 'aws-amplify';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { APIService } from '../API.service';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition, } from '@angular/material/snack-bar';

@Component({
  selector: 'app-user-authentication',
  templateUrl: './user-authentication.component.html',
  styleUrls: ['./user-authentication.component.scss']
})
export class UserAuthenticationComponent {
  isLoginMode: boolean = true;
  isLoading: boolean = false;
  username: string = null;

  constructor(
    private route: Router,
    private api: APIService,
    private _snackBar: MatSnackBar){}

  onSwitchMode() {
    this.isLoginMode = !this.isLoginMode;
  }

  onSubmit(form: NgForm) {
    if (!form.valid) {
      return;
    }

    this.username = form.value.email;
    this.isLoading = true;

    const authInfo = {
      username: form.value.email,
      email: form.value.email,
      password: form.value.password
    }

    if (this.isLoginMode) {
      Auth.signIn(authInfo).then(user => {
        console.log(user);
        this.route.navigate(['/dashboard'])
      })
        .catch(err => {
          console.log(err);
          this.openSnackBar("An error occurred!", 'error');
        })
    } else {
      // signUp
      const authInfo = {
        username: form.value.email,
        password: form.value.password,
        attributes: {
            email: form.value.email          // optional
            // other custom attributes
        }
      }

      //Execute Signup process
      Auth.signUp(authInfo)
        .then(data => {
          console.log(data);
          this.isLoginMode = true;
          this.openSnackBar("Check your email to verify your account.", 'success');
        })
        .catch(err => console.log(err));
    }

    form.reset();
    this.isLoading = false;
  }

  onVerify(verifycode: HTMLInputElement) {
    // After retrieving the confirmation code from the user
    this.isLoading = true;
    Auth.confirmSignUp(this.username, verifycode.value, {
      // Optional. Force user confirmation irrespective of existing alias. By default set to True.
      forceAliasCreation: true
      }).then(data => {
        console.log(data)
        this.isLoginMode = true
      })
        .catch(err => {
          console.log(err);
          this.openSnackBar("An error occurred!", 'error');
        })
    this.isLoading = false;
  }

    openSnackBar(msg: string, type: string = 'error') {
    let duration = 5000;
    let h_pos: MatSnackBarHorizontalPosition = 'center';
    let v_pos: MatSnackBarVerticalPosition = 'top';
    switch(type) {
      case 'error': {
        duration = 5000;
        h_pos = 'center';
        v_pos = 'top';
        break;
      }
      case 'success': {
        duration = 5000;
        h_pos = 'center';
        v_pos = 'bottom';
        break;
      }
      case 'warn': {
        duration = 5000;
        h_pos = 'center';
        v_pos = 'bottom';
        break;
      }
      default: {
        duration = 5000;
        h_pos = 'center';
        v_pos = 'top';
        break;
      }
    };
    this._snackBar.open(msg, 'dismiss', {
      duration: duration,
      horizontalPosition: h_pos,
      verticalPosition: v_pos,
    });
  } // openSnackBar

}
