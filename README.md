# Realtorbot
TLP: White

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.9.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

# Initialize Amplify Environment
## Amplify Rewrites and redirects
When initializing the Amplify environment, as a Single Page App (SPA), a redirect entry needs to be added
in order to deal with weird bugs in the backed web server.

```JSON
{“source”: “</^[^.]+$|\.(?!(css|gif|ico|jpg|js|png|txt|svg|woff|ttf|map|json)$)([^.]+$)/>”, “status”: “200”, “target”: “index.html”, “condition”: null}
```

# Appendix
## TLP and Security Concerns
* amplify/team-provider-info.json contains secrets for Facebook and Google APIs

## Generate GraphQL statements

`amplify codegen`
